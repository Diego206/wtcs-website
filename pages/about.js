import Head from 'next/head'
import styled from 'styled-components'
import Banner from './components/Banner'

const Hero = styled.div`
  padding: 2rem 2rem;
  height: 100%;
  background: ${props => props.theme.secondary};
`;

const Heading = styled.h1`
  font-size: 2rem;
  font-weight: 400;
  text-align: center;
`;

const BannerContent = styled.div`
  margin-top: 2rem;
  margin-left: 2rem;
  margin-right: 2rem;

  @media (min-width: 600px) {
    margin-top: 2rem;
    margin-left: 10rem;
    margin-right: 10rem;
  }
  @media (min-width: 900px) {
    margin-top: 10rem;
    margin-left: 10rem;
    margin-right: 10rem;
  }
`;

export default function About() {
  return (
    <>
      <Head>
        <title>About</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Banner>
        <BannerContent>
        <Heading>About Us</Heading>
        <div className='row'>
          <div className='col3'></div>
          <div className='col6'>
            <p></p>
            <p> WTCS Enterprises Pty Ltd is a South African Credit Guarantees Company that provides exceptional service in the field of Construction performance guarantees. Furthermore we also provide various surety services via our specialised and user friendly process. WTCS Enterprises Pty Ltd was established in early 2020  and began operation in early 2021 with the vision to become the number one construction insurer in South Africa. </p>
            <p> We believe in empowering outreach and growth development and therefore support upcoming young construction companies who like  us have the potential to grow from strength to strength </p>
            <p> Our Team and simple  process enables equal access to credit guarantee opportunities for construction businesses within the structured legislative framework of the National Credit Act. We believe that it is very important for contractors to receive the right information and guidance to make rates.  decisions that will have their best interest at heart.</p>
            <p> Our products are designed to assist construction businesses obtain guarantees in a simple and affordable manner.</p>
            <p> We are  committed to Exceptional service and the need of our clients will always be at the heart of what we do.</p>
          </div>
          <div className='col3'></div>
         </div>
         </BannerContent>
       </Banner>
    </>
  )
}
