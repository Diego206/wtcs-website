import React from 'react'
import Head from 'next/head'
import Banner from '../components/Banner';
import styled from 'styled-components';

const Content = styled.div`
  margin: 3rem 0; 
`;
 

export default function PerformanceGuarantee() {
  return (
    <>
      <Head>
        <title>Performance Guarantee</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Banner>
        <h1>Performance</h1>
        <h1>Guarantee</h1>
      </Banner>
      <Content className='row'>
        <div className='col-2'></div>
        <div className='col-8'>
          <h4>
            Normally issued for an amount equal to between 5 and 10 percent of the contract value. This guarantee assures payment to the employer in the event that the contractor fails to fulfill contract obligations.
          </h4>
        </div>
        <div className='col-2'></div>
      </Content>
    </>
  )
}


