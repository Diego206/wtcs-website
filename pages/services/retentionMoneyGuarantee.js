import React from 'react'
import Head from 'next/head'
import Banner from '../components/Banner';
import styled from 'styled-components';

const Content = styled.div`
  margin: 3rem 0; 
`;
 

export default function RetentionMoneyGuarantee() {
  return (
    <>
      <Head>
        <title>Advance Payment Guarantee</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Banner>
        <h1>Retention</h1>
        <h1>Money</h1>
        <h1>Guarantee</h1>
      </Banner>
      <Content className='row'>
        <div className='col-2'></div>
        <div className='col-8'>
          <h4>
            Most major projects call for stage payments as work progresses. Often the employer retains a percentage of the payment (retention money), as cover for any hidden defects in the completed work. A Retention Money Guarantee allows for immediate release of retention money to the contractor. The employer can get a refund of retention money released, in the event of default by the contractor.
          </h4>
        </div>
        <div className='col-2'></div>
      </Content>
    </>
  )
}


