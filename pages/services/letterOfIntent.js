import React from 'react'
import Head from 'next/head'
import Banner from '../components/Banner';
import styled from 'styled-components';

const Content = styled.div`
  margin: 3rem 0; 
`;
 

export default function LetterOfIntent() {
  return (
    <>
      <Head>
        <title>Letter Of Intent</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Banner>
        <h1>Letter</h1>
        <h1>Of</h1>
        <h1>Intent</h1>
      </Banner>
      <Content className='row'>
        <div className='col-2'></div>
        <div className='col-8'>
          <h4>
            A letter of intent (LOI) is a document declaring the preliminary commitment of one party to do business with another. This service is free of charge
          </h4>
        </div>
        <div className='col-2'></div>
      </Content>
    </>
  )
}


