import React from 'react'
import Head from 'next/head'
import Banner from '../components/Banner';
import styled from 'styled-components';

const Content = styled.div`
  margin: 3rem 0; 
`;
 

export default function AdvancePaymentGuarantee() {
  return (
    <>
      <Head>
        <title>Advance Payment Guarantee</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Banner>
        <h1>Advance</h1>
        <h1>Payment</h1>
        <h1>Guarantee</h1>
      </Banner>
      <Content className='row'>
        <div className='col-2'></div>
        <div className='col-8'>
          <h4>
            This enables the employer to get a refund of advance payments made in the event of default by the contractor. It is issued for the full amount of the advance payment, but may contain reduction clauses, which enable a reduction in the maximum amount upon evidence of progressive performance.
          </h4>
        </div>
        <div className='col-2'></div>
      </Content>
    </>
  )
}


