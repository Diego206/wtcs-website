import { ThemeProvider } from 'styled-components';
import Layout from './components/Layout'

const theme = {
  primary: '#1b2631',
  secondary: '#f5f6fa',
  links: '#F79F1F'
}

function MyApp({ Component, pageProps }) {
  return (
  <ThemeProvider theme={theme}>
    <Layout>
      <Component {...pageProps} />
    </Layout>
  </ThemeProvider>
  )
}

export default MyApp
