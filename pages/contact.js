import React, { useState} from 'react'
import Head from 'next/head'
import Banner from './components/Banner';
import styled from 'styled-components';
import { useForm } from 'react-hook-form';
import axios from 'axios';

const Content = styled.div`
  background: ${props => props.theme.secondary};
  color: ${props => props.theme.primary};
  padding: 1rem 1rem; 
  display: flex;
  flex-direction: column;

  button {
    background-color: ${props => props.theme.primary};
    color: ${props => props.theme.secondary};
  }

  h4 {
    padding-top: 2rem;
    color: #218c74;
  }

  h6 {
    color: #218c74;
  }

  @media (min-width: 600px) {
    padding: 3rem 5rem; 
    button {
      background-color: ${props => props.theme.primary};
      color: ${props => props.theme.secondary};
    }
  }
  @media (min-width: 900px) {
    padding: 3rem 10rem; 
  }

`;
 
const InputGroup = styled.div`
  padding: 1rem 0;

  small {
    color: #d63031;
  }
`;


export default function Contact() {

  const { register, handleSubmit, errors, reset} = useForm();
  const [success, setSuccess] = useState(false);

  const onSubmitForm = async (values) => {
    let config = {
      method: 'post',
      url: `/api/contact`,
      headers: { 'Content-Type': 'application/json' },
      data: values
    }
    
  const successDisplayTimer = () => {
    setSuccess(true);
    reset();
    setTimeout(() => {
      setSuccess(false);
    }, 8000);
  }
    try {
      const res = await axios(config);
      if(res.status == 200){
        successDisplayTimer();
      }
    } catch (error) {
      reset();
      console.error(error);
    }
    
  }

  return (
    <>
      <Head>
        <title>Contact</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Banner>
        <h1>Get</h1>
        <h1>In</h1>
        <h1>Touch</h1>
      </Banner>
      <Content>
        <form onSubmit={handleSubmit(onSubmitForm)}>
          <InputGroup>
            <label htmlFor="name"><strong>Your Name</strong></label>
            <input type="text" className="form-control" placeholder="Full Name" name="name" aria-describedby="name" 
                    ref={register({
                      required: { value: true, message: "Full Name is required"}
                    })} />
            <small id="name" className="form-text">
              { errors?.name?.message} 
            </small>
          </InputGroup>   
          <InputGroup>
            <label htmlFor="email"><strong>Your Email</strong></label>
            <input type="text" className="form-control" placeholder="Email Address" id="email" name="email" 
                    ref={register({
                      required: { value: true, message: 'Email is required'},
                      pattern: { value: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/ , message: "Invalid email addresss"}
                    })}/>
            <small id="email" className="form-text">
                { errors?.email?.message} 
            </small>
          </InputGroup>  
          <InputGroup>                    
            <label htmlFor="message"><strong>Your Enquiry</strong></label>
            <textarea className="form-control" placeholder="Message" id="message" name="message"
                      ref={register({
                        required: { value: true, message: 'Enquiry message is required'}
                      })}>
            </textarea>
            <small id="message" className="form-text">
                { errors?.message?.message} 
            </small>
          </InputGroup>           
          <div className='row'>
            <div className='col-1'>
              <button type="submit" className="btn">Submit</button>
            </div>
            <div className='col-11'></div>
          </div>
          {
          success ?
            <>
            <h4> Success !</h4>
            <h6>Your message has been sent we will contact you shortly</h6>
            </>
          :
            <div></div>
          }
        </form>
      </Content>
    </>
  )
}


