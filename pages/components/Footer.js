import styled from 'styled-components'
import { FaPhone } from "react-icons/fa"
import { IoLogoWhatsapp } from 'react-icons/io'
import {MdSmartphone} from 'react-icons/md'
import {HiOutlineMail} from 'react-icons/hi'

const FooterMain = styled.div`
  background: ${props => props.theme.primary};
  color: ${props => props.theme.secondary};
  
`;

const FooterSection = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 1rem 5rem;
  border-bottom: 2px solid;
  border-image:linear-gradient(to right, rgba(44, 62, 80,0),rgba(44, 62, 80,1), rgba(44, 62, 80,1),rgba(44, 62, 80,1), rgba(44, 62, 80,0)) 1;
`;

const FooterHeader = styled.h4`
  text-align: center;
`;

const FooterInfo = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding-top: 0.3rem;
`;

const FooterText = styled.h6`
  text-align: center;
  padding-left: 0.5rem;
`;

const FooterInfoIcon = styled.div`
  color: ${props => props.theme.links};
`;

const Logo = styled.div``;

const Footer = () => {
  return (
    <FooterMain className='container-fluid'>
      <div className='row'>        
        <FooterSection className='col'>
          <Logo>
            <img src='/images/logo.png' />
          </Logo> 
          <FooterHeader>WTCS Enterprises Pty Ltd</FooterHeader>         
          <FooterInfo>           
            <FooterText>117 Holzgen Street</FooterText>
          </FooterInfo>
          <FooterInfo>           
            <FooterText>Brackenhurst Ext 1</FooterText>
          </FooterInfo>
          <FooterInfo>           
            <FooterText>Alberton</FooterText>
          </FooterInfo>
          <FooterInfo>           
            <FooterText>1448</FooterText>
          </FooterInfo>
        </FooterSection>
      </div>
      <div className='row'>
        <FooterSection className='col'>
          <FooterHeader>Info</FooterHeader>
          <FooterInfo>
            <FooterInfoIcon><FaPhone /> </FooterInfoIcon>
            <FooterText>068 585 5673</FooterText>
          </FooterInfo>
          <FooterInfo>
            <FooterInfoIcon><IoLogoWhatsapp /> </FooterInfoIcon>
            <FooterText>068 585 5673</FooterText>
          </FooterInfo>
          <FooterInfo>
            <FooterInfoIcon><MdSmartphone /> </FooterInfoIcon>
            <FooterText>068 585 5673</FooterText>
          </FooterInfo>
          <FooterInfo>
            <FooterInfoIcon><HiOutlineMail /> </FooterInfoIcon>
            <FooterText>info@wtcs.co.za</FooterText>
          </FooterInfo>
        </FooterSection>
        <FooterSection className='col'>
          <FooterInfo>
            <FooterText>Copyright 2021 WTCS Enterprises Pty Ltd</FooterText>
          </FooterInfo>
          <FooterInfo>
            <FooterText>All rights reserved</FooterText>
          </FooterInfo>
          <FooterInfo>
            <FooterText>Resgistration Number: 2020/475211/07</FooterText>
          </FooterInfo>
          <FooterInfo>
            <FooterText>Resgistered National Credit Provider</FooterText>
          </FooterInfo>
          <FooterInfo>
            <FooterText>NCRCP8341</FooterText>
          </FooterInfo>
        </FooterSection>
      </div>
    </FooterMain>
  )
}

export default Footer
