
import Head from 'next/head'
import Footer from "./Footer"
import Navbar from "./Navbar"
import styled from 'styled-components';

const LayoutDiv = styled.div`
  overflow: hidden;
`;

export default  function Layout({children}){
  return (
    <LayoutDiv>
      <Head>     
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" />
      </Head>
      <Navbar />
        {children}
      <Footer />
    </LayoutDiv>
  );
}