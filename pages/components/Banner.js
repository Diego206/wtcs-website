import React from 'react'
import styled from 'styled-components';

const BannerDiv = styled.div`
  background-image: url('/images/banner.jpg');
  background-size: cover;
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
    p {
      color: ${props => props.theme.primary};
      padding: 10px;
    }

  @media (min-width: 600px) {
    height: 100%;
  }
  @media (min-width: 900px) {
    height: 400px;
  }
`;

const Banner = ({children}) => {
  return (
    <BannerDiv>
      { children }
    </BannerDiv>
  )
}

export default Banner
