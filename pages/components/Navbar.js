import react, { useState } from 'react';
import styled from 'styled-components';
import Link from 'next/link';
import breakpoint from '../../styles/breakpoints';
import { GiHamburgerMenu } from 'react-icons/gi';
import { FiX } from 'react-icons/fi';

const Nav = styled.nav`
  height: 80px;
  background: ${props => props.theme.primary}; 
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const StyledLink = styled.a`
  padding: 1rem 2rem;
  text-decoration: none;
  color:  ${props => props.theme.links};
  :hover {
    text-decoration: none;
    background:  ${props => props.theme.secondary}; 
    border-radius: 75px;
  }
`;

const Menu = styled.div`
  display: none;
  @media (min-width: 600px) {
    display: flex;
  }
  @media (min-width: 900px) {
    display: flex;
  }
`;

const MobileMenuToggle = styled.div`
  padding: 0rem 2rem;
  color: ${props => props.theme.links};

  @media (min-width: 600px) {
    display: none;
  }
  @media (min-width: 900px) {
    display: none;
  }
`;

const MobileMenu = styled.div`
  width: 100%;
  height: 60px;
  background: ${props => props.theme.primary};
  display: flex;
  flex-direction: row;
  align-items: center;
  border-top: 2px solid;
  border-image:linear-gradient(to right, rgba(44, 62, 80,0), rgba(44, 62, 80,1), rgba(44, 62, 80,0)) 1;
`;

const Logo = styled.div``;

function Navbar() {

  const [toggleMenu, setToggleMenu] = useState(false)

  return (
    <>
    <Nav>
      <MobileMenuToggle onClick={() => setToggleMenu(!toggleMenu)}>
        <h2>  { toggleMenu ? <FiX /> : <GiHamburgerMenu />} </h2>
      </MobileMenuToggle>
      <Menu>
        <Link href="/" passHref>
          <StyledLink>Home</StyledLink>
        </Link>
        <Link href="/about" passHref>
          <StyledLink>About</StyledLink>
        </Link>
        <Link href="/contact" passHref>
          <StyledLink>Contact</StyledLink>
        </Link>
      </Menu>
      <Logo>
        <Link href="/" passHref>
          <img src='/images/logo.png' />
        </Link>
      </Logo>    
    </Nav>
    { 
      toggleMenu ? 
      <MobileMenu>
        <Link href="/" passHref>
          <StyledLink>Home</StyledLink>
        </Link>
        <Link href="/about" passHref>
          <StyledLink>About</StyledLink>
        </Link>
        <Link href="/contact" passHref>
          <StyledLink>Contact</StyledLink>
        </Link>
      </MobileMenu> : <></>
      }
  </>
  )
}

export default Navbar
