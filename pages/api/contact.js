import nodemailer from 'nodemailer';

export default async (req, res) => {
  
  const { name, email,  message} = req.body;

  const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    post: '465',
    secure: true,
    auth: {
      user: process.env.username,
      pass: process.env.password
    }
  });

  try {
    const msg = await transporter.sendMail({
      from: email,
      to: 'blueline.care.connect@gmail.com',
      subject: `Wtcs Contact Form from ${name}`,
      html: `<p>Contact form submission</p></br>
      <p><strong>Name: </strong> ${name}</p></br>
      <p><strong>Email: </strong> ${email}</p></br>
      <p><strong>Message: </strong> ${message}</p></br>`
    })
    res.status(200).json(req.body);
  } catch (error) {
    console.error(error);
  }
  
  res.status(200).json(req.body);
}