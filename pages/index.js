import Image from 'next/image'
import Head from 'next/head'
import styled from 'styled-components';
import Link from 'next/link';
import Banner from './components/Banner';

const Hero = styled.div`
  height: 100%;
  padding: 0 2rem;
  background: ${props => props.theme.secondary};
  color: ${props => props.theme.primary};
`;

const ImgWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin: -40px 0;
`;

const ImgDiv = styled.div`
  display: flex;
  position: relative;
  align-items: center;
  width: 300px;
  height: 300px;

  img {
    border-radius: 20px;
    filter: brightness(.80);
    :hover {
      filter: brightness(.30);
    }
  }

  @media (min-width: 600px) {
    width: 450px;
    height: 450px;
  }
  @media (min-width: 900px) {
    width: 600px;
    height: 600px;
  }
`;

const Txt = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 2;
  transition: all ease-in-out;

  h5 {
    font-size: 1rem;
    text-align: center;
    color: #fff;
    font-weight: 300;

    :hover {
      font-weight:900;
    }
    @media (min-width: 600px) {
      font-size: 2rem;
    }
    @media (min-width: 900px) {
      font-size: 2rem;
    } 
  }
`;

const Heading = styled.div`
  font-size: 2rem;
  font-weight: 400;
  text-align: center;
  padding-top: 1rem;
`;


export default function Home() {
  return (
    <>
      <Head>
        <title>Home</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Banner>
        <p className="h1">WTCS Enterprises Pty Ltd</p>
        <p className="h4">Simple Solutions Exceptional Service</p>
        <p className="h6">
          We believe that building lasting relationships with our clients and partners is of utmost importance. 
          We also believe that we will become the number one guarantees company in SA by providing the most user friendly 
          process and exceptional speedy service.
        </p>
      </Banner>
      <Hero className='container-fluid'>
        <Heading>Our Services</Heading>        
        <div className="row">
          <ImgWrapper className="col">
            <ImgDiv>
              <img src="/images/img_1.jpg"  className='img-fluid' />
              <Txt className="col-6">
                <Link href='/services/letterOfIntent' passHref>
                  <h5>Letters Of Intent</h5>
                </Link>
              </Txt>
            </ImgDiv>     
          </ImgWrapper>
          <ImgWrapper className="col">
            <ImgDiv>
              <img src="/images/img_2.jpg"  className='img-fluid' />
              <Txt className="col-6">
                <Link href='/services/performanceGuarantee' passHref>
                  <h5>Performance Guarantee</h5>
                </Link>
              </Txt>
            </ImgDiv>              
          </ImgWrapper>
          <ImgWrapper className="col">
            <ImgDiv>
              <img src="/images/img_3.jpg"  className='img-fluid' />
              <Txt className="col-6">
                <Link href='/services/advancePaymentGuarantee' passHref>
                  <h5>Advance Payment Guarantee</h5>
                </Link>                
              </Txt>
            </ImgDiv>              
          </ImgWrapper>
          <ImgWrapper className="col">
            <ImgDiv>
              <img src="/images/img_4.jpg"  className='img-fluid' />
              <Txt className="col-6">
                <Link href='/services/retentionMoneyGuarantee'>
                  <h5>Retention Money Guarantee</h5>
                </Link>              
              </Txt>
            </ImgDiv>              
          </ImgWrapper>
        </div>
        <div className='row'>
          <div className='col-2'></div>
            <div className='col-8'>
              <p>
                All our guarantees are structured under secured credit facilities and credit agreements. 
                As part of our requirements it is important that assets provided as security are properly insured and is a condition 
                of our credit facility agreements. 
                Where insurance related cover is required MoeQuin industries (Pty) Ltd can refer you to a large panel of
                Financial Services Providers that can assist with Asset Insurance, Contract Works Insurance, and 
                Liability Insurance covers etc to meet our security requirements.
              </p>
            </div>
          <div className='col-2'></div>
        </div>
        <Heading>Our Strengths</Heading>
        <div className='row'>
          <div className='col-2'></div>
          <div className='col8'>
            <ul>
              <li>We are young and passionate about what we do</li>
              <li>Leadership in product innovation</li>
              <li>We believe in nurturing long-term relationships with our clients</li>
            </ul>
          </div>
          <div className='col-2'></div>
        </div>
        <Heading>Our Promise</Heading>
        <div className='row'>
          <div className='col-2'></div>
          <div className='col8'>
            <ul>
              <li>Service that is accurate and fair.</li>
              <li>Innovation.</li>
              <li>Fast and effective service.</li>
              <li>Simplicity.</li>
            </ul>
          </div>
          <div className='col-2'></div>
        </div>
      </Hero>
    </>
  )
}
